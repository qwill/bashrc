# Quest's Bash Config -- intended for use with OSX Iterm2
# https://gitlab.com/qwill/bashrc

# Disable OSX warning about "default shell is now zsh"
export BASH_SILENCE_DEPRECATION_WARNING=1

# Fresh OSX install steps
# 1) install homebrew
# 2) brew install coreutils -- brings in GNU "sort" used by this prompt for quotes
# 3) brew install git bash-completion -- brings in "git_ps1" used for displaying current branch
# 4) brew install cowsay - pulls in both cowsay & cowthink
#    brew install lolcat - terminal colorization (used for cowsay prompt)
# 5) iTerm2 menu -> "Install Shell Integration"
PATH_TO_COWFILES="/usr/local/Cellar/cowsay/3.04/share/cows/"
# from github.com/paulkaefer/cowsay-files/tree/master/cows
# ran command to get files with <= 31 lines:
# find . -type f -exec awk -v x=32 'NR==x{exit 1}' {} \; -exec cp {} ~/repos/qwill/bashrc/extra-cows/. \;
# then put into my own extra-cows folder
cd $HOME
PATH_TO_EXTRA_COWFILES="$HOME/repos/qwill/bashrc/extra-cows/"

# ---~~~=== STATIC DEFINITIONS ===~~~---
# only known to work on OSX. Sets the title of the iTerm2 session tab
function title {
    echo -ne "\033]0;"$*"\007"
}
# reloads the .bash_profile (which should be symlinked to .bashrc) into the current shell
function reload {
    source ~/.bash_profile
}

# all tabs are titled "free" and set to green by default
title free
echo -ne "\033]6;1;bg;red;brightness;188\a"
echo -ne "\033]6;1;bg;green;brightness;213\a"
echo -ne "\033]6;1;bg;blue;brightness;96\a"

HOSTNAME=qlaptop6
QHOST_NAME=$HOSTNAME
TERMINAL_SESSION_ID=$(uuidgen | tr -d '\n' | tr '[:upper:]' '[:lower:]')

printf "%b" "\e[0;90m\$TERMINAL_SESSION_ID ${TERMINAL_SESSION_ID}\e[0m"

# Boot (Clojure) JVM Options
BOOT_JVM_OPTIONS="-client -XX:+TieredCompilation -XX:TieredStopAtLevel=1
  -Xmx2g -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -Xverify:none"

# Various qlaptop5 folder aliases
alias qkeyboard="cd ~/Dropbox/PRJ/qmk_firmware"
alias -- make-qkeyboard="cd ~/Dropbox/projects/qmk_firmware && sudo make keyboard=ergodox keymap=ambi-macs"
alias cdu="cd ../"
alias cdb="cd -"
alias cdh="cd ~"

alias uuid="uuidgen | tr -d '\n' | tr '[:upper:]' '[:lower:]' | pbcopy && pbpaste && echo"

# OSX ls coloring
export CLICOLOR=1
export LSCOLORS=ExFxCxDxBxegedabagacad

# OSX "Open Finder in current folder"
alias finder="open ."

# utility commands to launch Android virtual device from cmd-line
export ANDROID_SDK=$HOME/Library/Android/sdk
export ANDROID_HOME=$ANDROID_SDK
export PATH=$ANDROID_SDK/emulator:$ANDROID_SDK/tools:$PATH
alias -- avd-list="cd ~/Library/Android/Sdk/tools/bin && ./avdmanager list avd"
alias -- avd-launch="cd ~/Library/Android/Sdk/tools && ./emulator -avd Pixel_2_API_28"

# ----------------~~~~~~~~======== BASH PROMPT ========~~~~~~~~----------------
PROMPT_COMMAND='export PREV_CMD_STATUS=$?'

# example of using if built-in to PS1 in order to display different prompts
# https://github.com/ryanoasis/public-bash-scripts/blob/master/ultimate-git-ps1-bash-prompt.sh
# if can get the if working, can use `git describe --always`
PS1="\[\e[0m\]\[\033[35m\]\t\[\033[m\] \[\033[36m\]\w\[\e[m\]\n\[\033[35m\]\u@$QHOST_NAME\[\e[m\] \`if [ \$PREV_CMD_STATUS = 0 ]; then echo \[\033[32m\]\(^_^\)\>\[\e[0m\]; else echo \[\e[31m\]\(O_O\)\>\[\e[0m\]; fi\` "

if [ -f $(brew --prefix)/etc/bash_completion ]; then
   . $(brew --prefix)/etc/bash_completion
   GIT_PS1_SHOWDIRTYSTATE=true
   PS1="\[\e[0m\]\[\033[35m\]\t\[\033[m\] \[\033[36m\]\$(__git_ps1 "{%s}")\w\[\e[m\]\n\[\033[35m\]\u@$QHOST_NAME\[\e[m\] \`if [ \$PREV_CMD_STATUS = 0 ]; then echo \[\033[32m\]\(^_^\)\>\[\e[0m\]; else echo \[\e[31m\]\(O_O\)\>\[\e[0m\]; fi\` "
fi

# ----------------~~~~~~~~======== Emacs vterm ========~~~~~~~~----------------
# recommended functions to enable vterm integration in Emacs
# see https://github.com/akermu/emacs-libvterm#directory-tracking-and-prompt-tracking
if [[ "$INSIDE_EMACS" = 'vterm' ]];
then
    printf "\n\e[90mEnabling Emacs vterm integration... !\e[0m\n"
    function clear(){
        vterm_printf "51;Evterm-clear-scrollback";
        tput clear;
    }

    function vterm_printf(){
	if [ -n "$TMUX" ]; then
            # Tell tmux to pass the escape sequences through
            # (Source: http://permalink.gmane.org/gmane.comp.terminal-emulators.tmux.user/1324)
            printf "\ePtmux;\e\e]%s\007\e\\" "$1"
	elif [ "${TERM%%-*}" = "screen" ]; then
            # GNU screen (screen, screen-256color, screen-256color-bce)
            printf "\eP\e]%s\007\e\\" "$1"
	else
            printf "\e]%s\e\\" "$1"
	fi
    }

    function vterm_prompt_end(){
	vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
    }
    PS1=$PS1'\[$(vterm_prompt_end)\]'
else
    # optionally enables iterm2_shell_integration if it's available
    # note that this causes extra character issues with Emacs builtin ansi-term
    # https://randre03.github.io/terminal_prompt_in_emacs
    test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
fi

# ----------------~~~~~~~~======== ENV ========~~~~~~~~----------------
export PATH=$HOME/Library/Python/2.7/bin:$PATH
export PATH=$HOME/bin:$PATH
export PATH=$HOME/Library/Android/sdk/platform-tools:$PATH
export JAVA_HOME=/Library/Java/JavaVirtualMachines/openjdk-14.jdk/Contents/Home
#export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_251.jdk/Contents/Home

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/quest/Downloads/google-cloud-sdk/path.bash.inc' ]; then source '/Users/questy/Downloads/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/quest/Downloads/google-cloud-sdk/completion.bash.inc' ]; then source '/Users/questy/Downloads/google-cloud-sdk/completion.bash.inc'; fi

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

if [ -x "$(command -v git)" ]; then
    line="$(sort -R ~/repos/qwill/quotes/quotes.edn | head -n1)"
    printf "\n"
    if (( RANDOM % 2 )); then
	echo ${line} | cowsay -f `ls ${PATH_TO_COWFILES}/*.cow ${PATH_TO_EXTRA_COWFILES}*.cow | grep -v -e "telebears" -e "sodomized" | shuf -n1` | lolcat; else
	echo ${line} | cowthink -f `ls ${PATH_TO_COWFILES}/*.cow ${PATH_TO_EXTRA_COWFILES}*.cow | grep -v -e "telebears" -e "sodomized" | shuf -n1` | lolcat;
    fi
    printf "\n"
fi

# required for GPG to open correct password prompt terminal
export GPG_TTY=$(tty)

# ----------------~~~~~~~~======== ANSIBLE VAULT ========~~~~~~~~----------------
export ANSIBLE_VAULT_PASS_DIR="$HOME/.vault"
export ANSIBLE_VAULT_QA="ANSIBLE_VAULT_PASSWORD_FILE=\"${ANSIBLE_VAULT_PASS_DIR}/qa.txt\""
export ANSIBLE_VAULT_GOV="ANSIBLE_VAULT_PASSWORD_FILE=\"${ANSIBLE_VAULT_PASS_DIR}/gov.txt\""
export ANSIBLE_VAULT_PROD="ANSIBLE_VAULT_PASSWORD_FILE=\"${ANSIBLE_VAULT_PASS_DIR}/prod.txt\""
export ANSIBLE_VAULT_CHINA="ANSIBLE_VAULT_PASSWORD_FILE=\"${ANSIBLE_VAULT_PASS_DIR}/china.txt\""
alias ansible-playbook-qa="${ANSIBLE_VAULT_QA} ansible-playbook"
alias ansible-playbook-gov="${ANSIBLE_VAULT_GOV} ansible-playbook"
alias ansible-playbook-prod="${ANSIBLE_VAULT_PROD} ansible-playbook"
alias ansible-playbook-china="${ANSIBLE_VAULT_CHINA} ansible-playbook"
alias ansible-vault-qa="${ANSIBLE_VAULT_QA} ansible-vault"
alias ansible-vault-gov="${ANSIBLE_VAULT_GOV} ansible-vault"
alias ansible-vault-prod="${ANSIBLE_VAULT_PROD} ansible-vault"
alias ansible-vault-china="${ANSIBLE_VAULT_CHINA} ansible-vault"

# ----------------~~~~~~~~======== CONCUR SPECIFIC ========~~~~~~~~----------------
alias ususersvcsaws="docker run --rm -it -v /Users/i856165/.aws/:/root/.aws quay.cnqr.delivery/containerhosting/aws-okta-auth -a 0oamkpbhy42QcFaQF0x7/272 -o concurasp -u quest.yarbrough -n USUserSvcsProfile -r --region us-west-2"

###################################################################################
# if you launch Emacs via normal shortcut, doesn't inherit normal shell env
# which means things like `vterm` won't see executables like `lein`
# Launching it from a shell via nohup makes it inherit env which fixes this
alias emacs-with-env="nohup /Applications/Emacs.app/Contents/MacOS/Emacs &"

# enables bash completion for kubectl
source <(kubectl completion bash)
alias kubectl-contexts="kubectl config view -o jsonpath='{.contexts[*].name}' && echo"

# may need to add to path for SSL within minikube
# /usr/local/opt/openssl/bin:$PATH

# may need to add to flags for compilers to find openssl
#export LDFLAGS="-L/usr/local/opt/openssl/lib"
#export CPPFLAGS="-I/usr/local/opt/openssl/include"
